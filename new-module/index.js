/*jshint node: true*/

'use strict';
var yeoman = require('yeoman-generator'),
	chalk = require('chalk');

module.exports = yeoman.generators.Base.extend({
	initializing: function () {
		var that = this,
			greetingLines = that.config.get('fbizGreetLines');
		that.answers = that.config.get('answers');

		that.config.set({
			modulesPath: that.config.get('mainFolder') + '/' + that.config.get('assetsPath') + '/sass/source/modules',
			wrappersPath: that.config.get('mainFolder') + '/' + that.config.get('assetsPath') + '/sass/source/wrappers'
		});

		if (greetingLines && greetingLines.length) {
			greetingLines.forEach(function(v) {
				that.log(v);
			});
		} else {
			that.log(chalk.red('ERROR: ') + '.yo-rc.json' + chalk.red(' config file not found, maybe first you have to run ') + 'yo hyojun-bootstrap');
			return;
		}
	},

	prompting1: function () {
		var that = this,
			done = that.async(),
			prompts;

		//yeoman uses https://github.com/SBoudrias/Inquirer.js
		prompts = [{
				type: 'input',
				name: 'moduleName',
				message: 'New module name:',
				default: 'my-new-module'
			}
		];

		that._answers(prompts, function () {
			done();
		});
	},

	prompting2: function() {
		var that = this,
			done = that.async(),
			locations = that.expand(that.config.get('modulesPath') + '/*'),
			prompts;

		that.newFolderValue = '[CREATE NEW FOLDER]';

		locations.push(that.newFolderValue);

		prompts = [{
			type: 'list',
			name: 'location',
			message: 'Where do you want to place it?',
			choices: locations
		},{
			type: 'input',
			name: 'newFolder',
			message: 'New folder name:',
			default: 'my-new-folder',
			when: function(currAnswers) {
				return currAnswers.location === that.newFolderValue;
			}
		}];

		that._answers(prompts, function () {
			done();
		});
	},

	prompting3: function() {
		var that = this,
			done = that.async(),
			locations = that.expand(that.config.get('wrappersPath') + '/*'),
			prompts;

		that.newWrapperValue = '[CREATE NEW WRAPPER FILE]';

		locations.push(that.newWrapperValue);

		prompts = [{
			type: 'checkbox',
			name: 'wrappers',
			message: 'In which wrapper(s) do you want to add it?',
			choices: locations
		},{
			type: 'input',
			name: 'newWrapper',
			message: 'New wrapper\'s file name:',
			default: 'my-new-wrapper',
			when: function(currAnswers) {
				return currAnswers.wrappers && currAnswers.wrappers.indexOf(that.newWrapperValue) > -1;
			}
		}];

		that._answers(prompts, function () {
			done();
		});
	},

	_answers: function (prompts, callback) {//task not called automatically
		var that = this;
		that.prompt(prompts, function (answers) {
			for (var key in answers) {
				that.answers.data[key] = answers[key];
			}
			callback();
		}.bind(that));
	},

	writingNewModule: function () {
		var that = this,
			newModulePath = that.answers.data.location,
			wrapperPath,
			file;

		if (newModulePath === that.newFolderValue) {
			newModulePath = that.config.get('modulesPath') + '/' + that.answers.data.newFolder;
		}

		newModulePath += '/' + that.answers.data.moduleName;

		that.fs.copyTpl(
			that.templatePath(that.config.get('modulesPath') + '/newModule.scss'),
			that.destinationPath(newModulePath + '.scss'),
			that.answers
		);

		that.answers.data.wrappers.forEach(function(v) {

			file = '';

			if (v === that.newWrapperValue) {
				wrapperPath = that.destinationPath(that.config.get('wrappersPath') + '/' + that.answers.data.newWrapper + '.scss');
			} else {
				wrapperPath = that.destinationPath(v);
				file = that.read(wrapperPath);
			}

			file += that.engine(that.read(that.templatePath(that.config.get('wrappersPath') + '/_wrappers.scss')), {path: newModulePath.split('source')[1]});

			that.write(wrapperPath, file);
		});

	},
});
