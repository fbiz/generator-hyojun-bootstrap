/*jshint node: true, camelcase: false, loopfunc: true */

'use strict';
var yeoman = require('yeoman-generator'),
	chalk = require('chalk'),
	exec = require('child_process').exec,
	cpr = require('cpr');

module.exports = yeoman.generators.Base.extend({

	initializing: function () {

		var that = this;

		that.config.set({
			configPath: 'config/',
			assetsPath: 'assets/',
			viewsPath: 'views/',
			cachePath: '_cached',
			cloneRepo: 'hyojun.bootstrap-template',
			gitSystemFolder: '.git',
			mainFolder: 'WWW'
		});

		that.answers = {
			data: { //for underscore templates
				columns: {}
			},
		};

		that.mediaChoices = { //questions info, will be modified when necessary
			'mobile': {
				name:'Mobile',
				value: {width: 320},
				checked: true,
				columnChoices: [
					{name:'2', checked: true},
					{name:'custom'}
				]
			},
			'tablet': {
				name:'Tablet',
				value: {width: 768},
				checked: true,
				columnChoices: [
					{name:'4', checked: true},
					{name:'8'},
					{name:'custom'}
				]
			},
			'desktop': {
				name:'Desktop',
				value: {width: 960},
				checked: true,
				columnChoices: [
					{name:'10'},
					{name:'12', checked: true},
					{name:'custom'}
				]
			},
			'wide': {
				name:'Desktop Wide',
				value: {width: 1306},
				checked: true,
				columnChoices: [
					{name:'10'},
					{name:'12', checked: true},
					{name:'custom'}
				]
			}
		};

		that.chosen = [];

	},

	fbizGreet: function () {
		var that = this,
			lines = [];
		lines.push(chalk.yellow('   ___'));
		lines.push(chalk.yellow(' /' + chalk.bgYellow('     ') + '\\   _________________________________'));
		lines.push(chalk.yellow('|') + chalk.black(chalk.bgYellow(' F.biz ')) + chalk.yellow('|  |   Hyojun Boostrap Generator   |'));
		lines.push(chalk.yellow(' \\' + chalk.bgYellow('     ') + '/   ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯'));
		lines.push(chalk.yellow('   ¯¯¯'));
		that.config.set({
			fbizGreetLines: lines
		});
		lines.forEach(function(v) {
			that.log(v);
		});
	},

	checkFolders: function() {
		var that = this,
			mainFolder = that.expand(that.config.get('mainFolder'));
		if (!mainFolder.length) {
			that._cloneRepo();
		}
	},

	checkRepo: function() {
		var that = this,
			gitFolder = that.expand(that.config.get('gitSystemFolder'));
		if (!gitFolder.length) {
			that._promptRepoInfo();
		}
	},

	prompting1: function () {

		var that = this,
			done = that.async(),
			prompts,
			id,
			choices = [];

		//yeoman uses https://github.com/SBoudrias/Inquirer.js
		prompts = [{
				type: 'input',
				name: 'projectName',
				message: 'Project name:',
				default: 'my-project'
			},
			{
				type: 'confirm',
				name: 'responsive',
				message: 'Is this a responsive project?',
				default: true
			},
			{
				type: 'checkbox',
				name: 'breakpoints',
				message: 'Do you want to add media query breakpoints? (all selected by default)',
				choices: function () {
					for (id in that.mediaChoices) {//transforms object to array to input to inquirer.js
						that.mediaChoices[id].value.id = id;
						that.mediaChoices[id].name += ' (' + that.mediaChoices[id].value.width + 'px)';
						choices.push(that.mediaChoices[id]);
					}
					return choices;
				},
				when: function (answers) {
					return answers.responsive;
				}
			}
		];

		that._answers(prompts, function () {
			done();
		});
	},

	prompting2: function () {

		var that = this,
			prompts = [],
			done = that.async();

		if (that.answers.data.breakpoints && that.answers.data.breakpoints.length) {

			that._.each(that.mediaChoices, function (v, id) {

				that.answers.data.breakpoints.every(function(v, i) {
					if (v.id === id) { //checks which media is being chosen and marks it
						that.mediaChoices[id].chosen = true;
						v.breakpoint = 'auto';
						if (i !== that.answers.data.breakpoints.length - 1) { //if not the last one, sets the breakpoint as the (width of the next one - 1)
							v.breakpoint = that.answers.data.breakpoints[i + 1].width - 1 + 'px';
						}
						return false;
					}
					return true;
				});

				prompts.push({ //adds column number questions (with custom) if media is chosen
					type: 'list',
					name: 'columns_' + id,
					message: 'How many columns do you want for ' + v.name + '?',
					choices: v.columnChoices,
						when: function() {//it is not executed yet
						//console.log('id:', id);//prints the last one in "for in", and everyone in "foreach"
						return that.mediaChoices[id].chosen;
					}
				}, {
					type: 'input',
					name: 'columns_' + id,
					message: 'Enter column number for ' + v.name + ':',
					validate: function(v) {
						if (!isNaN(v) && v !== '') {
							return true;
						}
						return 'Please enter valid number.';
					},
					when: function(currAnswers) {
						return currAnswers['columns_' + id] === 'custom';
					}
				});

			});

			that._answers(prompts, function () {
				done();
			});

		} else {
			done();
		}
	},

	persistingAnswers: function () {
		var that = this;
		that.config.set({
			answers: that.answers
		});
	},

	writingGridTemplate: function () {
		var that = this,
			path = that.config.get('mainFolder') + '/' + that.config.get('assetsPath') + 'sass/source/core/config/_grids.scss';
		that.fs.copyTpl(
			that.templatePath(path),
			that.destinationPath('/' + path),
			that.answers
		);
	},

	done: function() {
		this.log('generator done');
	},

	//Auxiliary methods
	_promptRepoInfo: function () {
		var that = this,
			done = that.async(),
			prompts,
			id,
			choices = [];

		//yeoman uses https://github.com/SBoudrias/Inquirer.js
		prompts = [{
				type: 'input',
				name: 'repoName',
				message: 'Repository name:',
				default: 'my-project'
			},
			{
				type: 'input',
				name: 'repoUrl',
				message: 'Repository remote url:',
				default: 'http://bitbucket/fbiz/my-project.git'
			},
		];

		that.prompt(prompts, function (answers) {
			that._initRepo(answers.repoName, answers.repoUrl, done);
		}.bind(that));
	},

	_initRepo: function(repoName, repoUrl, done) {
		var that = this;

		exec('git init && git remote add ' + repoName + ' ' + repoUrl,
			function(error, stdout, stderr) {
				that._execCallback(error, stdout, stderr);
				done();
			}
		);
	},

	_cloneRepo: function() {
		var that = this,
			done = that.async();
		that.log('Cloning ' + chalk.yellow('hyojun.bootstrap-template') + ' repository...');
		exec('mkdir _cached && git clone http://bitbucket.org/fbiz/' + that.config.get('cloneRepo') + '.git ' + that.config.get('cachePath') + '/' + that.config.get('cloneRepo'),
			function(error, stdout, stderr) {
				that._execCallback(error, stdout, stderr);
				that._extractFiles(function() {
					done();
				});
			}
		);
	},

	_extractFiles: function(callback) {
		var that = this;
		cpr(that.config.get('cachePath') + '/' + that.config.get('cloneRepo'), '.', {
				deleteFirst: false,
				overwrite: false,
				confirm: true,
				filter: function(element, index, array) {
					if(element.indexOf('.git/') > -1 || (element.indexOf(that.config.get('gitSystemFolder')) < -1 && element.length === that.config.get('gitSystemFolder').length)) {
						return false;
					}
					return true;
				}
			},
			function(err, files) {
				if (err) {
					that.log(chalk.red('Extract Files: ' + err));
				}
				that._deleteDirectory(that.config.get('cachePath'), function() {
					callback();
				});
			}
		);
	},

	_deleteDirectory: function(dirpath, done) {
		var that = this;
		exec('rm -rf ' + dirpath,
			function (error, stdout, stderr) {
				that._execCallback(error, stdout, stderr);
				done();
			}
		);
	},

	_execCallback: function(error, stdout, stderr) {
		var that = this;
		if (stdout) {
			that.log('stdout: ' + stdout);
		}
		// if (stderr) {
		// 	that.log('stderr: ' + stderr);
		// }
		if (error !== null) {
			this.log(chalk.red(error));
		}
	},

	_answers: function (prompts, callback) {//task not called automatically
		var that = this;
		that.prompt(prompts, function (answers) {
			for (var key in answers) {
				if (key.indexOf('columns_') > -1) {
					that.answers.data.columns[key.split('_')[1]] = answers[key];
				} else {
					that.answers.data[key] = answers[key];
				}
			}
			callback();
		}.bind(that));
	},


});