# Hyojun Bootstrap Generator docs

## Install yeoman CLI
	sudo npm install -g yeoman

## Go to your new project folder and install this generator from npm
	sudo npm install git+https://bitbucket.org/fbiz/generator-hyojun-bootstrap#working

## Generate project:
	yo hyojun-bootstrap

## To create new module:
	yo hyojun-bootstrap:new-module